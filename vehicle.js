class Vehicle{
    constructor(licenseNumber,color,manufacturer){
        this.licenseNumber = licenseNumber;
        this.color = color;
        this.manufacturer = manufacturer;
    }

    drive(speed){
        console.log('Driving at speed ', speed);
    }

    reverse(){
        console.log('reverse');
    }
}
module.exports = Vehicle;