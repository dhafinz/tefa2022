const Vehicle = require('./vehicle');

function main(){
    let myVehicle = new Vehicle('D 1234 AZ','Black','Toyota');
    console.log('My Vehicle Number License is', myVehicle.licenseNumber);

    myVehicle.drive()
    myVehicle.reverse()
}

main()
